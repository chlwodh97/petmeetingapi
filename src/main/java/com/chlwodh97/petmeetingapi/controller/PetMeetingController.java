package com.chlwodh97.petmeetingapi.controller;


import com.chlwodh97.petmeetingapi.model.petmeeting.PetMeetingItem;
import com.chlwodh97.petmeetingapi.model.petmeeting.PetMeetingNameRequest;
import com.chlwodh97.petmeetingapi.model.petmeeting.PetMeetingRequest;
import com.chlwodh97.petmeetingapi.model.petmeeting.PetMeetingResponse;
import com.chlwodh97.petmeetingapi.service.PetMeetingService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/v1/pet-meeting")
@RequiredArgsConstructor
public class PetMeetingController {
    private final PetMeetingService petMeetingService;

    @PostMapping("/join")
    public String setPetMeeting(@RequestBody PetMeetingRequest request){
        petMeetingService.setPetMeeting(request);

        return "오키오키";

    }

    @GetMapping("/list")
    public List<PetMeetingItem> setPetMeetings(){
        return petMeetingService.setPetMeetings();
    }

    @GetMapping("/detail/{id}")
    public PetMeetingResponse setPetMeeting(@PathVariable long id){
        return petMeetingService.setPetMeeting(id);
    }

    @PutMapping("{id}")
    public String putPetMeeting(@PathVariable long id , @RequestBody PetMeetingNameRequest request){
        petMeetingService.putPetMeeting(id, request);

        return "수정되었습니다";
    }

}
