package com.chlwodh97.petmeetingapi.controller;


import com.chlwodh97.petmeetingapi.entity.PetMeeting;
import com.chlwodh97.petmeetingapi.model.mypet.MyPetRequest;
import com.chlwodh97.petmeetingapi.service.MyPetService;
import com.chlwodh97.petmeetingapi.service.PetMeetingService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/my-pet")
public class MyPetController {
    private final MyPetService myPetService;
    private final PetMeetingService petMeetingService;

    @PostMapping("/new/pet-meeting-id/{petmeetingId}")
    public String setMyPet(@PathVariable long petmeetingId ,@RequestBody MyPetRequest request){

        PetMeeting petMeeting = petMeetingService.getData(petmeetingId);
        myPetService.setMyPet(request , petMeeting);
        return  "okok";
    }
}
