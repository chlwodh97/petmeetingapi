package com.chlwodh97.petmeetingapi.model.petmeeting;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetMeetingNameRequest {
    private String name;
}
