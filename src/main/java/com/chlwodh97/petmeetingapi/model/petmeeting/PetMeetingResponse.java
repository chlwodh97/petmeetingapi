package com.chlwodh97.petmeetingapi.model.petmeeting;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetMeetingResponse {
    private Long id;
    private String name;
    private Short age;
    private String isManName;
    private String phoneNumber;
}
