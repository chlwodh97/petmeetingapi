package com.chlwodh97.petmeetingapi.model.petmeeting;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetMeetingItem {
    private Long id;
    private String name;
    private Boolean isMan;
}
