package com.chlwodh97.petmeetingapi.model.petmeeting;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetMeetingRequest {
    private String name;
    private Short age;
    private Boolean isMan;
    private String phoneNumber;
}
