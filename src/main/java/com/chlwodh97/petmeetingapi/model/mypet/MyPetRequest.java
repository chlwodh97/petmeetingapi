package com.chlwodh97.petmeetingapi.model.mypet;


import com.chlwodh97.petmeetingapi.entity.PetMeeting;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MyPetRequest {
    private PetMeeting petMeeting;
    private String petName;
    private String petType;
}
