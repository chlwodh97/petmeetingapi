package com.chlwodh97.petmeetingapi.repository;

import com.chlwodh97.petmeetingapi.entity.PetMeeting;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetMeetingRepository extends JpaRepository<PetMeeting, Long> {
}
