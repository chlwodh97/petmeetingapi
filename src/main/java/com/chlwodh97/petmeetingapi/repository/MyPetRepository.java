package com.chlwodh97.petmeetingapi.repository;

import com.chlwodh97.petmeetingapi.entity.MyPet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MyPetRepository extends JpaRepository<MyPet ,Long> {
}
