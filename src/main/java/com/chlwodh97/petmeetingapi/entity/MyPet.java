package com.chlwodh97.petmeetingapi.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Lazy;

@Entity
@Getter
@Setter
public class MyPet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "petMeetingId")
    @ManyToOne(fetch = FetchType.LAZY)
    private PetMeeting petMeeting;

    @Column(nullable = false)
    private String petName;

    @Column(nullable = false)
    private String petType;
}
