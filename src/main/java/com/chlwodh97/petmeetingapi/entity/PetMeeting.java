package com.chlwodh97.petmeetingapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class PetMeeting {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Short age;

    @Column(nullable = false)
    private Boolean isMan;

    @Column(nullable = false , unique = true)
    private String phoneNumber;
}
