package com.chlwodh97.petmeetingapi.service;


import com.chlwodh97.petmeetingapi.entity.MyPet;
import com.chlwodh97.petmeetingapi.entity.PetMeeting;
import com.chlwodh97.petmeetingapi.model.mypet.MyPetRequest;
import com.chlwodh97.petmeetingapi.repository.MyPetRepository;
import com.chlwodh97.petmeetingapi.repository.PetMeetingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MyPetService {
    private final MyPetRepository myPetRepository;

    public void setMyPet(MyPetRequest request , PetMeeting petMeeting) {
        MyPet addData = new MyPet();

        addData.setPetName(request.getPetName());
        addData.setPetType(request.getPetType());
        addData.setPetMeeting(petMeeting);

        myPetRepository.save(addData);
    }
}
