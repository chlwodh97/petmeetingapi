package com.chlwodh97.petmeetingapi.service;


import com.chlwodh97.petmeetingapi.entity.PetMeeting;
import com.chlwodh97.petmeetingapi.model.petmeeting.PetMeetingItem;
import com.chlwodh97.petmeetingapi.model.petmeeting.PetMeetingNameRequest;
import com.chlwodh97.petmeetingapi.model.petmeeting.PetMeetingRequest;
import com.chlwodh97.petmeetingapi.model.petmeeting.PetMeetingResponse;
import com.chlwodh97.petmeetingapi.repository.PetMeetingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PetMeetingService {
    private final PetMeetingRepository petMeetingRepository;

    public PetMeeting getData(long id){
        return petMeetingRepository.findById(id).orElseThrow();
    }

    public void setPetMeeting(PetMeetingRequest request){

        PetMeeting addData = new PetMeeting();
        addData.setName(request.getName());
        addData.setAge(request.getAge());
        addData.setIsMan(request.getIsMan());
        addData.setPhoneNumber(request.getPhoneNumber());

        petMeetingRepository.save(addData);
    }

    public List<PetMeetingItem> setPetMeetings() {

        List<PetMeeting> originList = petMeetingRepository.findAll();

        List<PetMeetingItem> result = new LinkedList<>();

        for (PetMeeting petMeeting : originList){
            PetMeetingItem addItem = new PetMeetingItem();
            addItem.setId(petMeeting.getId());
            addItem.setName(petMeeting.getName());
            addItem.setIsMan(petMeeting.getIsMan());

            result.add(addItem);
        }
        return result;
    }

    public PetMeetingResponse setPetMeeting(long id) {

        PetMeeting originData = petMeetingRepository.findById(id).orElseThrow();

        PetMeetingResponse response = new PetMeetingResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setAge(originData.getAge());
        response.setIsManName(originData.getIsMan()? "남자" : "여자");
        response.setPhoneNumber(originData.getPhoneNumber());

        return response;
    }

    public void putPetMeeting(long id , PetMeetingNameRequest request) {
        PetMeeting putData = petMeetingRepository.findById(id).orElseThrow();
        putData.setName(request.getName());

        petMeetingRepository.save(putData);

    }

}
